<?php

class conf {

	/*############################*/
	/*######### ATTRIBUTS ########*/
	/*############################*/

	// Language.
	private $lang = 'fr';

	// Titre.
	private $title = 'SuiviSIO';

	// Mode débugage.
	private $debug = true;

	/*########################
	#	Thème graphique :	#
	#	- cosmos			#
	#	- cyborg			#
	#	- lumen				#
	#	- darkly			#
	#########################*/
	private $theme = 'cyborg';

	/*### Connexion à la base de donnée ###*/

	// Driver (pilote) SGBD disponible : PDO, MysqlMysqli, Mysql, Postgresql, Odbc, Sqlite
	private $driver = 'PDO';

	// Domain du serveur la base de donnée :
	private $host = 'localhost';

	// Nom de l'utilisateur de la base de donnée :
	private $user = 'forum';

	// Mot de passe de l'utilisateur de la base de donnée :
	private $password = 'forum';

	// Nom de la base de donnée :
	private $db = 'analyse';

	// Nombres d'élements max par liste :
	private $pagination = '10';

	/*########################*/
	/*####### METHODES #######*/
	/*########################*/

	// Getters
	public function getLang() {
		return $this->lang;
	}
	public function getDebug() {
		return $this->debug;
	}
	public function getTheme() {
		return $this->theme;
	}
	public function getHost() {
		return $this->host;
	}
	public function getUser() {
		return $this->user;
	}
	public function getPassword() {
		return $this->password;
	}
	public function getDB() {
		return $this->db;
	}
	public function getTitle() {
		return $this->title;
	}
	public function getDriver() {
		return $this->driver;
	}
	public function getPagination() {
		return $this->pagination;
	}

	// Setters
	private function setLang($lang) {
		$this->lang = $lang;
	}
	private function setDebug($debug) {
		$this->debug = $debug;
	}
	private function setTheme($theme) {
		$this->theme = $theme;
	}
	private function setHost($host) {
		$this->host = $host;
	}
	private function setUser($user) {
		$this->user = $user;
	}
	private function setPassword($password) {
		$this->password = $password;
	}
	private function setDB($db) {
		$this->db = $db;
	}
	private function setPagination($pagination) {
		$this->pagination = $pagination;
	}

};

// Création de l'objet si include dans un fichier.
$config = new conf();

global $config;

?>
