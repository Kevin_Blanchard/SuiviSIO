<?php
require_once(MODEL . 'Diplome.php');
require_once(ENGINE . 'Controllers/Controller.php');

class DiplomesController extends Controller {
	private $options;

	public function __construct($options) {
		$this->options = $options;
	}

	public function start() {

		// Vérification de l'existence ou pas de l'option ID.
		$test = strpos($this->options, '/');
		// soit c'est un id soit c'est une new page.
		if($test == TRUE) {
			// pour un unique metier.
			if(strpos($this->options, 'id') === 0) {
				$id = $this->slashitation($this->options, '/');

				$leDiplome = new Diplome($this->options);
				$leDiplome = $leDiplome->getUnDiplome('id = '.$id[1]);

				include(PAGE . 'Diplomes/unDiplome.php');
			}

			// pour une new page.
			if(strpos($this->options, 'page') === 0) {
				$page = $this->slashitation($this->options, '/');
				$this->viewLesDiplomes();
			}
		}
		else {
			$this->viewLesDiplomes();
		}
	}

	public function viewLesDiplomes() {
		$lesDiplomesObject = new Diplome($this->options);
		$lesDiplomes = $lesDiplomesObject->getLesDiplomes('1');
		$leNombreDePages = $lesDiplomesObject->getNbPageDiplomes();
		$controller = 'Diplomes';

		include(PAGE . 'Diplomes/listeDesDiplomes.php');
	}

}

$Diplome = new DiplomesController($this->options);
$Diplome->start();

?>
