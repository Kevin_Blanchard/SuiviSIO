<?php
require_once(MODEL . 'Discipline.php');
require_once(ENGINE . 'Controllers/Controller.php');

class DisciplinesController extends Controller {
	private $options;

	public function __construct($options) {
		$this->options = $options;
	}

	public function start() {

		// Vérification de l'existence ou pas de l'option ID.
		$test = strpos($this->options, '/');
		// soit c'est un id soit c'est une new page.
		if($test == TRUE) {
			// pour un unique metier.
			if(strpos($this->options, 'id') === 0) {
				$id = $this->slashitation($this->options, '/');

				$leDiscipline = new Discipline($this->options);
				$leDiscipline = $leDiscipline->getUnDiscipline('id = '.$id[1]);

				include(PAGE . 'Disciplines/unDiscipline.php');
			}

			// pour une new page.
			if(strpos($this->options, 'page') === 0) {
				$page = $this->slashitation($this->options, '/');
				$this->viewLesDisciplines();
			}
		}
		else {
			$this->viewLesDisciplines();
		}
	}

	public function viewLesDisciplines() {
		$lesDisciplinesObject = new Discipline($this->options);
		$lesDisciplines = $lesDisciplinesObject->getLesDisciplines('1');
		$leNombreDePages = $lesDisciplinesObject->getNbPageDisciplines();
		$controller = 'Disciplines';

		include(PAGE . 'Disciplines/listeDesDisciplines.php');
	}

}

$Discipline = new DisciplinesController($this->options);
$Discipline->start();

?>
