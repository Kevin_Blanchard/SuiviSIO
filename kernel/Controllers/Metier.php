<?php
require_once(MODEL . 'Metier.php');
require_once(ENGINE . 'Controllers/Controller.php');

class MetierController extends Controller {
	private $options;

	public function __construct($options) {
		$this->options = $options;
	}

	public function start() {

		// Vérification de l'existence ou pas de l'option ID.
		$test = strpos($this->options, '/');
		// soit c'est un id soit c'est une new page.
		if($test == TRUE) {
			// pour un unique metier.
			if(strpos($this->options, 'id') === 0) {
				$id = $this->slashitation($this->options, '/');

				$leMetier = new Metier($this->options);
				$leMetier = $leMetier->getUnMetier('id = '.$id[1]);

				include(PAGE . 'Metier/unMetier.php');
			}

			// pour une new page.
			if(strpos($this->options, 'page') === 0) {
				$page = $this->slashitation($this->options, '/');
				$this->viewLesMetiers();
			}
		}
		else {
			$this->viewLesMetiers();
		}
	}

	public function viewLesMetiers() {
		$lesMetiersObject = new Metier($this->options);
		$lesMetiers = $lesMetiersObject->getLesMetiers('1');
		$leNombreDePages = $lesMetiersObject->getNbPageMetier();
		$controller = 'Metier';

		include(PAGE . 'Metier/listeDesMetiers.php');
	}

}

$Metier = new MetierController($this->options);
$Metier->start();

?>
