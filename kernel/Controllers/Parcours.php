<?php
require_once(MODEL . 'Parcours.php');
require_once(ENGINE . 'Controllers/Controller.php');

class ParcoursController extends Controller {
	private $options;

	public function __construct($options) {
		$this->options = $options;
	}

	public function viewLesParcours() {
		$lesParcours = new Parcours($this->options);
		$lesParcours = $lesParcours->getLesParcours();
		include(PAGE . 'listeDesParcours.php');
	}

}

$parcours = new ParcoursController($this->options);
$parcours->viewLesParcours();

?>
