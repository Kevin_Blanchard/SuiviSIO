<?php
require_once(MODEL . 'Secteur.php');
require_once(ENGINE . 'Controllers/Controller.php');

class SecteurController extends Controller {
	private $options;

	public function __construct($options) {
		$this->options = $options;
	}

	public function start() {

		// Vérification de l'existence ou pas de l'option ID.
		$test = strpos($this->options, '/');
		if($test == TRUE) {
			$id = $this->slashitation($this->options, '/');

			$leSecteur = new Secteur($this->options);
			$leSecteur = $leSecteur->getUnSecteur($id[1]);

			require_once(MODEL . 'Metier.php');
			$lesMetiers = new Metier($this->options);
			$lesMetiers = $lesMetiers->getLesMetiers("secteur_id = $id[1]");

			include(PAGE . 'Secteur/unSecteur.php');
		}
		else {
			$this->viewLesSecteurs();
		}
	}

	public function viewLesSecteurs() {
		$lesSecteurs = new Secteur($this->options);
		$lesSecteurs = $lesSecteurs->getLesSecteurs();

		include(PAGE . 'Secteur/listeDesSecteurs.php');
	}

}

$secteur = new SecteurController($this->options);
$secteur->start();

?>
