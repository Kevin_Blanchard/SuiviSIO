<?php
require_once(MODEL . 'Universite.php');
require_once(ENGINE . 'Controllers/Controller.php');

class UniversiteController extends Controller {
	private $options;

	public function __construct($options) {
		$this->options = $options;
	}

	public function viewLesUniversite() {
		$lesUniv = new Universite($this->options);
		$lesUniv = $lesUniv->getLesUniversite();
		include(PAGE . 'listeDesUniversite.php');
	}

}

$secteur = new UniversiteController($this->options);
$secteur->viewLesUniversite();

?>
