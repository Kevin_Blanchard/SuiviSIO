<?php
require_once(ENGINE . 'Drivers/Functions.php');
require_once(ENGINE . 'Controllers/Controller.php');

class AdministrationController extends Controller {
	private $options;
	
	public function __construct($options) {
		$this->options = $options;
	}
	
	public function index() {
		if($_SESSION['right'][0]['admin'] == 1) {
			
			echo $this->options;
			
			// Si option renvoie une sous-action
			if((!empty($this->options)) || ($this->options == '')) {
				// Découpage de la chaîne si l'option est pas vide
				$test = strpos($this->options, '/');
				if(!empty($test)) {
					$tab = $this->slashitation($this->options, '/');
				}
			}
			if(empty($tab[0])){$tab[0] ='';};
			
			switch($tab[0]) {
				case '' :
					$this->listTables();
					break;
				case 'Add' :
					$this->add($tab[1]);
					break;
				case 'PostAdd' :
					$this->postAdd($tab[1]);
					break;
				case 'Remove' :
					$this->remove($tab[1]);
					break;
				case 'Updated' :
					$this->updated($tab[1]);
					break;
			}
		}
		else {
			echo 'Pas la permission';
		}
	}
	
	public function add($table) {
		require_once(ENGINE . 'Models/Droit.php');
		$Droits = new Droit($this->options);
		$columns = $Droits->getColumnTable($table);
		$columns = $columns->fetchAll();
		
		include(TEMPLATE . 'themeAdmin.php');
		include(TEMPLATE . 'add.php');
	}
	
	public function postAdd($table) {
		
		require_once(ENGINE . 'Models/Droit.php');
		$Droits = new Droit($this->options);
		
		$lesColumns = $Droits->getColumnTable($table);
		
		foreach($lesColumns as $uneLigne) {
			$lesValeurs = $lesValeurs . ', ' . $uneLigne['Field'];
		}
		
		echo '<pre>';
		echo $lesValeurs;
		
		print_r($_POST);
		
		echo '</pre>';
		
		$columns = $Droits->insertInto($lesColumns, $table, $values);
	}
	
	public function remove($table, $id) {
		include(TEMPLATE . 'themeAdmin.php');
		include(TEMPLATE . 'remove.php');
	}
	
	public function updated($table, $id) {
		include(TEMPLATE . 'themeAdmin.php');
		include(TEMPLATE . 'updated.php');
	}
	
	public function listTables() {
		global $config;
		require_once(ENGINE . 'Models/Droit.php');
		$Droits = new Droit($this->options);
		$lesDroits = $Droits->getLesDroits();
		
		$lesTables = $Droits->getListTable();
		
		// le diviseur
		$nbElementParPage = $config->getPagination();
		
		foreach($lesTables as $unTable) {
			// on calcule la limite à atteindre en SQL
			$limit = $Droits->paginatationSQL($this->options);
			
			// on compte le nombre d'élément dans la table
			$countElement = $Droits->select_simple('count(*)', $unTable[0]);
			$countElement = $countElement->fetch();
			$countElement = $countElement[0];
			
			// on calcule le nombre de pages pour la pagination
			if($countElement > $nbElementParPage) {
				// le nombre d'élément par page est plus grand, donc pagination
				$leNombreDePages[] = ceil($countElement/$nbElementParPage);
			}
			else {
				$leNombreDePages[] = 0;
			}
			// on cherche les éléments de la base
			$lesDatasTable = $Droits->select('*', $unTable[0], '1 = 1', 'id', $limit);
			$lesAutres[] = $lesDatasTable->fetchAll();
		}
		include(TEMPLATE . 'themeAdmin.php');
		include(TEMPLATE . 'admin.php');
	}
}

$pageAdmin = new AdministrationController($this->tab[1]);
$pageAdmin->index();

?>
