<?php

class Controller {

	// Methods

	/* Gestion des slashs
	 * return : Array (preValue, postValue)
	*/
	public function slashitation($string, $key) {

		// Vérification
		if(!empty($string) && !empty($key)) {
			// Je cherche où doit s'arrêter ma chaîne pour le premier caractère.
			// Action/Login
			//       ^
			$numeroKeyPosition = mb_stripos($string, $key, 0);

			// J'extrai le nom de l'action demander.
			// Action/Login
			// ******
			$preValue = substr($string, 0, $numeroKeyPosition);

			// Je cherche le nombre de caractère dans la chaîne pour en extraire le reste.
			// Action/Login
			// 12
			$nbCharacterString = strlen($string);

			// J'extrai à partir du premier slash.
			// Action/Login
			//        *****
			$postValue = substr($string, $numeroKeyPosition+1, $nbCharacterString);

			$tableau = Array($preValue, $postValue);
		}
		else {
			echo "Erreur : Controleur.php : ";
			$tableau = null;
		}
		return $tableau;
	}

	// Liste des 30 premiers résultats
	public function listController($table) {

		$PDO = new SQL();

		$query = $PDO->select(
		'*', // Select
		'users', // From
		"1", // Where
		"id", // Order by
		"30"); // Limit

		$result = $query->fetchAll();

		$count = $query->rowCount();
		echo '<pre>';
		print_r($result);
		echo "# Il y a $count lignes #";
		echo '</pre>';
	}

}

?>
