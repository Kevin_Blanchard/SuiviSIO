<?php

	// CONSTANTES
	define("WEBROOT", str_replace('index.php', '', $_SERVER['SCRIPT_NAME']));
	define("ROOT", str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']));

	// Front
	define("CSS", WEBROOT."front/css/");
	define("JS", WEBROOT."front/js/");
	define("FONTS", WEBROOT."front/fonts/");
	define("PICTURES", WEBROOT."front/pictures/");

	// Kernel
	define("ENGINE", ROOT."kernel/Engine/");

	define("MODEL", ROOT."kernel/Models/");

	define("CONTROLLER", ROOT."kernel/Controllers/");

	define("VUE", ROOT."kernel/Views/");
	define("PAGE", ROOT."kernel/Views/Pages/");
	define("TEMPLATE", ROOT."kernel/Views/Templates/");

	// Configuration
	define("CONFIG", ROOT."config/");

	/* Fichier principale de configuration */
	include(CONFIG . 'config.php');

?>
