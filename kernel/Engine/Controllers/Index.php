<?php

	include('Define.php');

	/* Par défaut les Templates d'entête et de pied de page sont incluts */
	include(VUE . 'Templates/head.php');
	include(VUE . 'Templates/navigation.php');

	require(ENGINE . 'Controllers/Instance.php');
	$instance = new Instance();

	session_start();

	// Passage à la fonction d'action. Temporisation de la vue.
	ob_start();
	$instance->action();
	$temp = ob_get_contents();
	ob_end_clean();

	// Affichage de la vue du menu.
	ob_start();
	$instance->askConnect();
	$menu = ob_get_contents();
	ob_end_clean();

	echo $menu, $temp;

	ob_end_flush();

	/* Corps des pages */

	include(VUE . 'Templates/foot.php');

?>
