<?php
require(ENGINE . 'Drivers/'. $config->getDriver() .'.php');
require(ENGINE . 'Controllers/Controller.php');

class Instance extends Controller {
	// Attributs
	private $action;
	private $controller;
	private $options;
	private $url;
	private $tab;
	// Tableau des action du moteur
	private $tableAction;

	// Methods
	public function __construct() {
		$this->action		= null;
		$this->controller	= null;
		$this->options		= null;
		$this->url			= null;
		$this->tableAction	= Array("Login", "Logout", "Register", "User", "Administration");
	}

	public function action() {

		// Si get renvoie une demande de controller
		if((!empty($_GET['p'])) || ($_GET['p'] != '')) {

			// Premier découpage de la chaîne si un slash est trouver
			$test = strpos($_GET['p'], '/');
			if(!empty($test)) {
				$this->tab = $this->slashitation($_GET['p'], '/');
				$this->action = $this->tab[0];
			}

			// Recherche de l'action
			if($this->action != null || $this->action != '') {
				// Passage au controlleur et redéfinition de la chaîne à vérifier
				$this->tab = $this->tab[1];
				$this->controller();
			}
			else {
				echo "Erreur : Variable action vide";
			}
		}
		else {
			// Variable p vide, affichage de la page d'accueil
			if(isset($_SESSION['connected'])) {
				include(PAGE . 'Accueil/accueil.php');
				include(PAGE . 'Accueil/connecter.php');
			}
			else {
				include(PAGE . 'Accueil/accueil.php');
			}
		}
	}

	public function controller() {
		// Je cherche les options possibles.
		// Si un slash est tourver dans le reste de la chaine, on continue
		$test = strpos($this->tab, '/');
		if(!empty($test)) {

			$this->tab = $this->slashitation($this->tab, '/');

			$this->controller = $this->tab[0];
			$this->options = $this->tab[1];
		}
		else {
			$this->controller = $this->tab;
		}

		// Si in_array, true, sinon recherche du controller
		$test = in_array($this->controller, $this->tableAction);

		// Action connue du moteur -> Engine/Controllers/
		if(!empty($test)) {
			// Execution du controller en question
			switch($this->controller) {
				case 'Login':
					$this->login();
				break;
				case 'Logout':
					$this->logout();
				break;
				case 'Register':
					$this->register();
				break;
				case 'User':
					$this->user();
				break;
				case 'Administration':
					$this->administration();
				break;
			}
		}
		else {
			// Sinon, action de l'application -> Controller/<nom du controller>.php
			$filename = CONTROLLER . $this->controller .'.php';
			// Vérification si le fichier controller existe.
			if(file_exists($filename)) {
				// Execution du controller en question
				include($filename);
			}
			else {
				// A réécrire dans Error.php ?
				echo '<br><span>Veuillez créer le fichier "' . $filename . '"</span>';
			}
		}
	}

	public function askConnect() {
		/* Affichage des infos si connecter, sinon afficher les boutons de connexion */
		if(isset($_SESSION['connected'])) {
			if($_SESSION['connected'] === true) {
				include(TEMPLATE . 'online.php');
			}
		}
		else {
			include(TEMPLATE . 'offline.php');
		}
	}

	public function register() {
		// Si l'utilisateur est connecter
		if(isset($_SESSION['connected'])) {
			if($_SESSION['connected'] === TRUE) {
				// On affiche le template du déjà connecter.
				include(TEMPLATE . 'alreadyConnected.php');
			}
		}
		else {
			require(ENGINE . 'Controllers/Register.php');
			$register = new Register($this->options);
			$register->register();
		}
	}

	public function login() {
		// Si l'utilisateur est connecter
		if(isset($_SESSION['connected'])) {
			if($_SESSION['connected'] === true) {
				// On affiche le template du déjà connecter.
				include(TEMPLATE . 'alreadyConnected.php');
			}
		}
		else {
			require(ENGINE . 'Controllers/Login.php');
			$login = new Login();
			if($this->options == 'PostConnexion') {
				$login->postConnexion();
			}
			else {
				$login->login();
			}
		}
	}

	public function logout() {
		if($_SESSION['connected'] === true) {
			$_SESSION = false;
			session_destroy();
			include(PAGE . 'Accueil/accueil.php');
		}
		else {
			include(PAGE . 'Accueil/accueil.php');
		}
	}

	public function user() {
		// Si l'utilisateur est connecter
		if(isset($_SESSION['connected'])) {
			if($_SESSION['connected'] === TRUE) {
				// On affiche le template du déjà connecter.
				include(ENGINE . 'Controllers/User.php');
			}
		}
		else {
			include(TEMPLATE . 'login.php');
		}
	}

	public function administration() {
		if(isset($_SESSION['right'])) {
			if($_SESSION['right'][0]['admin'] == 1) {
				include(ENGINE . 'Controllers/Administration.php');
			}
		}
		else {
			echo 'Accès interdit !';
		}
	}

}

?>
