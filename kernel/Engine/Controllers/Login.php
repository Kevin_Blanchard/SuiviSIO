<?php

class Login {
	private $post;

	public function __construct() {
		$this->post		= $_POST;
	}

	public function login() {
		// Si l'utilisateur n'est pas connecter
		if(!isset($_SESSION['connected'])) {
			// On affiche le template si l'utilisateur ne renvoie pas de $_POST
			if(!isset($_POST['pseudo']) && !isset($_POST['mdp'])) {
				include(TEMPLATE . 'login.php');
			}
			else {
				// Pas l'un ou l'autre
			}
		}
		// L'utilisateur est déjà connecter
		else {
			include(TEMPLATE . 'alreadyConnected.php');
		}

	}

	public function postConnexion() {
		global $config;

		$included_files = get_included_files();
		if(!in_array(ENGINE . 'Drivers/'. $config->getDriver() .'.php', $included_files)) {
			require_once(ENGINE . 'Drivers/'. $config->getDriver() .'.php');
		}
		// Les deux conditions sont remplies
		if(isset($this->post['pseudo']) && isset($this->post['mdp'])) {

			$PDO = new SQL();

			$query = $PDO->select(
			'*', // Select
			'`users`', // From
			"(username = '" . $this->post['pseudo'] . "' OR email = '" .
			$this->post['pseudo'] . "') AND password = '" . md5($_POST['mdp']) . "'", // Where
			"id", // Order by
			"2"); // Limit

			$result = $query->fetchAll();

			$count = $query->rowCount();

			// Qu'une ligne trouver, on peut créer un objet User
			if($count === 1) {
				// id de l'utilisateur, pour récupérer les infos a posteriori
				$_SESSION['idUser']    = $result[0]['id'];
				$_SESSION['nameUser']  = $result[0]['username'];
				$_SESSION['connected'] = TRUE;

				$included_files = get_included_files();
				if(!in_array(ENGINE . 'Models/Droit.php', $included_files)) {
					require_once(ENGINE . 'Models/Droit.php');
				}

				$leDroitUser = new Droit(NULL);
				$_SESSION['right'] = $leDroitUser->getUnDroit($result[0]['id']);

				include(PAGE . 'Accueil/accueil.php');
				include(PAGE . 'Accueil/connecter.php');
			}
			else {
				echo "<div class=\"container\"><div class=\"page-header\"></div><span class=\"text-danger\">ERREUR :
						</span><span class=\"text-danger\">Votre pseudo ou/et mot de passe est/sont incorrect.</span>
					  </div>";
			}
		}
	}
}


?>
