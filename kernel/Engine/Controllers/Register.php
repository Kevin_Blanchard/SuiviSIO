<?php

class Register {
	private $options;
	
	public function  __construct($options) {
		$this->options = $options;
	}
	
	public function register() {
		if(!empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['confirmed'])) {
			$this->postRegister();
		}
		else {
			include(TEMPLATE . 'register.php');
		}
	}
	
	public function postRegister() {
		if($this->options == 'PostRegister') {
			// Protection des 3 champs.
			$formLogin = Array(
							'email' => $_POST['email'], 
							'password' => $_POST['password'], 
							'confirmed' => $_POST['confirmed']
						);

			require_once(ENGINE . 'Models/User.php');
			require_once(ENGINE . 'Drivers/PDO.php');
			
			// On execute une première requête pour 
			// vérifier que le nouvelle utilisateur 
			// ne se retrouve pas en double
			
			$PDO = new SQL();
			
			$query = $PDO->select('*', 'users', "email = '" . $formLogin['email'] . "'", "id", "1");
			
			$result = $query->fetchAll();
			
			$count = $query->rowCount();
			
			// Si l'utilisateur est déjà dans la base.
			if($count == 1) {
				echo "ERREUR : Pseudo déjà dans la base !";
			}
			else {
				// Vérification des deux mots de passe.
				if($formLogin['password'] === $formLogin['confirmed']) {
					
					// Création d'un objet utilisateur pour vérification des champs.
					$newUser = new User($formLogin['email'], time(), time(), $formLogin['password'], null, time());
					
					// On recherche les valeurs mise en forme de l'objet.
					$tableauUser = Array($newUser->getPseudo(), $newUser->getPassword(), $newUser->getCreated(), $newUser->getModified());
					
					// Insertion dans la base
					$query = $PDO->insertInto('email, password, created, modified', 'users', $tableauUser);
				}
			}
			include(TEMPLATE . 'accueil.php');
		}
		else {
			// Erreur
			echo "<div class=\"container\"><div class=\"page-header\"></div><span>ERREUR : Page non existante</span></div>";
		}
		
	}
}

?>
