<?php
require_once(ENGINE . 'Models/User.php');
require_once(ENGINE . 'Controllers/Controller.php');

class UserController extends Controller {
	private $options;

	public function __construct($options) {
		$this->options = $options;
	}

	// user a été trouver dans l'url
	public function actionUser() {

		$test = strpos($this->options, '/');

		// Si un nouveau slash est trouver, une action est invoquer.
		if($test == TRUE) {
			$actionUser = $this->slashitation($this->options, '/');
		}
		else { $actionUser = null; }

		// Gestion du profil
		if(strstr($this->options, 'Profil')) {

			// Si un fomulaire est envoyer
			$test = strpos($this->options, '/');

			// Sous-action du profil
			if(!empty($actionUser[1])) {
				switch($actionUser[1]) {
					case 'PostProfil':
						$this->updateUser();
						break;
					case 'RemoveUser':
						$this->deleteUser(1);
						break;
					case 'PostDeleteUser':
						$this->deleteUser(2);
						break;
				}
			}
			else {
				$this->viewLUser();
			}
		}
	}

	// Supression d'un user
	public function deleteUser($condition=NULL) {
		// valeur passer en parametre cacher
		if(empty($_SESSION['idDeSuppression'])) { $_SESSION['idDeSuppression'] = NULL; }

		if(($_SESSION['idDeSuppression'] == NULL) && ($condition == 1)) {
			// premier choix user
			$_SESSION['idDeSuppression'] = uniqid();
			include(TEMPLATE . 'acceptedDeleteUser.php');
		}
		else {
			if($_SESSION['idDeSuppression'] == $_POST['idDeSuppression']) {
				// les id sont vérifié, je peut supprimer
				// confirmation
				$user->deleteUser();
			}
			else {
				$_SESSION['idDeSuppression'] = NULL;
			}
		}
	}

	// Modification d'un user
	public function updateUser() {

		$user = new User($this->options);

		$user->updateUser($_POST['username'], $_POST['email'], $_POST['password'], $_POST['nom'], $_POST['prenom']);

		$this->viewLUser();

		include(TEMPLATE . 'confirmModificated.php');
	}

	public function viewLUser() {
		// Rechercher les info.
		$user = new User($this->options);

		// Chercher les info avec l'id
		$info = $user->getUnUser($_SESSION['idUser']);

		// remplacement des date par un format correcte
		$date = new DateTime($info[0]['modified']);
		$info[0]['modified'] = $date->format('à H:i:s');
		$info[0]['modified'] = $info[0]['modified'] . ' le ' . $date->format('d-m-Y');

		$date = new DateTime($info[0]['created']);
		$info[0]['created'] = $date->format('à H:i:s');
		$info[0]['created'] = $info[0]['created'] . ' le ' . $date->format('d-m-Y');

		// Affichage des infos du profil
		include(TEMPLATE . 'profil.php');
	}

	// Fonction qui retourne le nombre "droit"
	public function getRight() {
		$leDroit = new Droit($this->options);
		$leDroit = $leDroit->getUnDroit($_SESSION['idUser']);
		$_SESSION['right'] = $leDroit['admin'];
		return $leDroit['admin'];
	}
}

$pageUser = new UserController($this->options);
$pageUser->actionUser();

?>
