<?php

class SQL {
	private $sql;
	private $result;
	private $config;
	private $dsn;
	private $lastErrorNo;
	private $lastErrorMessage;
	private $query;
	

// Requirements :
//	$config['sql_type']
//	$config['sql_host']
//	$config['sql_port']
//	$config['sql_database']
//	$config['sql_user']
//	$config['sql_password']
//	$config['sql_fetch_type']

	public function __construct() {
		$this->config = $config;
		$this->_connect();
	}

	private function _connect($config) {
		global $config;
		if($config['sql_type'] == 'mysql') {
			try {
				$dsn = $config['sql_host'] . ':' . $config['sql_port'];
				mysql_connect($dsn, $config['sql_user'], $config['sql_password']);
				mysql_select_db($config['sql_database'], $this->sql);
			}
			catch {
				$this->lastErrorNo = mysql_errno($this->sql);
				$this->lastErrorMessage = mysql_error($this->sql);
				echo "<span>$this->lastErrorNo : $this->lastErrorMessage</span>";
			}
		}
		else {
			echo "<span>Aucun type de connection définie dans \$config\[\'sql_type\'\]</span>";
		}
	}

	public function destruct() {
		if($config['sql_type'] == 'mysql') {
			$this->_disconnect('mysql');
		}
		else {
			echo "<span>Aucun type de connection définie dans \$config\[\'sql_type\'\]</span>";
		}
	}

	private function _disconnect($type) {
		if($config['sql_type'] == 'mysql') {
			mysql_close($this->sql);
		}
	}

	public function execute($this->query) {
		if($config['sql_type'] == 'mysql') {
			mysql_query($this->query);
		}
	}

	public function fetch() {
		if($config['sql_type'] == 'mysql') {
			$this->result = mysql_fetch_row($this->query);
			return $this->result;
			/*
			*	mysql_​fetch_​array
			*	mysql_​fetch_​assoc
			*	mysql_​fetch_​field
			*	mysql_​fetch_​lengths
			*	mysql_​fetch_​object
			*	mysql_​fetch_​row
			 */
			switch($config['sql_fetch_type']) {
				case 'array':
					$this->result = mysql_​fetch_​array($this->query);
					return $this->result;
				break;
				case 'assoc':
					$this->result = mysql_​fetch_​assoc($this->query);
					return $this->result;
				break;
				case 'field':
					$this->result = mysql_​fetch_​field($this->query);
					return $this->result;
				break;
				case '​lengths':
					$this->result = mysql_​fetch_​lengths($this->query);
					return $this->result;
				break;
				case 'object':
					$this->result = mysql_​fetch_​object($this->query);
					return $this->result;
				break;
				case 'row':
					$this->result = mysql_​fetch_​row($this->query);
					return $this->result;
				break;
			}
		}
	}
}
