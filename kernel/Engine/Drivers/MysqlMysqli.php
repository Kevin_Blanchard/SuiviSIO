<?php

// Paramètres : pdo, mysqli, mysql
class SQL {
	private $sql;
	private $result;
	private $config;
	private $dsn;
	private $lastErrorNo;
	private $lastErrorMessage;
	private $query;

// Requirements :
//	$config['sql_type']
//	$config['sql_host']
//	$config['sql_port']
//	$config['sql_database']
//	$config['sql_user']
//	$config['sql_password']
//	$config['sql_fetch_type']

	public function __construct($config) {
		$this->config = $config;
		$this->_connect();
	}

	private function connect() {
		global $config;
		if($config['sql_type'] == 'mysqli') {
			try {
				$this->sql = new mysqli($config['sql_host'], $config['sql_user'], $config['sql_password'], $config['sql_database'], $config['sql_port']);
			}
			catch {
				$this->lastErrorNo = mysqli::$errno($this->sql);
				$this->lastErrorMessage = mysqli::$error($this->sql);
				echo "<span>$this->lastErrorNo : $this->lastErrorMessage</span>";
			}
		}
		else {
			echo "<span>Aucun type de connection définie dans \$config\[\'sql_type\'\]</span>";
		}
	}

	public function destruct() {
		if($config['sql_type'] == 'mysqli') {
				$this->_disconnect();
		}
		else {
			echo "<span>Aucun type de connection définie dans \$config\[\'sql_type\'\]</span>";
		}
	}

	private function _disconnect() {
		mysqli::close($this->sql);
	}

	public function execute($this->query) {
		if($config['sql_type'] == 'mysqli') {
			mysqli::query($this->query);
		}
	}

	public function fetch() {
		if($config['sql_type'] == 'mysqli') {
			/*
			*	mysqli_result::fetch_all — 			Lit toutes les lignes de résultats dans un tableau
			*	mysqli_result::fetch_array — 			Retourne une ligne de résultat sous la forme d'un tableau associatif, d'un tableau indexé, ou les deux
			*	mysqli_result::fetch_assoc — 			Récupère une ligne de résultat sous forme de tableau associatif
			*	mysqli_result::fetch_field_direct — 	Récupère les métadonnées d'un champ unique
			*	mysqli_result::fetch_field — 			Retourne le prochain champs dans le jeu de résultats
			*	mysqli_result::fetch_fields — 		Retourne un tableau d'objets représentant les champs dans le résultat
			*	mysqli_result::fetch_object — 		Retourne la ligne courante d'un jeu de résultat sous forme d'objet
			*	mysqli_result::fetch_row — 			Récupère une ligne de résultat sous forme de tableau indexé
			*/
			switch($config['sql_fetch_type']) {
				case 'all':
					$this->result = mysqli::fetch_all($this->query);
					return $this->result;
				break;
				case 'array':
					$this->result = mysqli_result::fetch_array($this->query);
					return $this->result;
				break;
				case 'assoc':
					$this->result = mysqli_result::fetch_assoc($this->query);
					return $this->result;
				break;
				case 'field_direct':
					$this->result = mysqli_result::fetch_field_direct($this->query);
					return $this->result;
				break;
				case 'field':
					$this->result = mysqli_result::fetch_field($this->query);
					return $this->result;
				break;
				case 'fields':
					$this->result = mysqli_result::fetch_fields($this->query);
					return $this->result;
				break;
				case 'object':
					$this->result = mysqli_result::fetch_object($this->query);
					return $this->result;
				break;
				case 'row':
					$this->result = mysqli_result::fetch_row($this->query);
					return $this->result;
				break;
			}
		}
	}
}
