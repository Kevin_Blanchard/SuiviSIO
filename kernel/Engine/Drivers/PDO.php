<?php

class SQL {
	private $PDO;
	private $PDOstatement;
	private $result;
	private $config;
	private $dsn;
	private $lastErrorNo;
	private $lastErrorMessage;
	private $query;

	public function __construct() {
		$this->_connect();
	}

	private function _connect() {
		global $config;
		if($config->getDriver() == 'PDO') {
			try {
				$dsn = 'mysql:dbname=' . $config->getDB() . ';host=' . $config->getHost();
				$this->PDO = new PDO($dsn, $config->getUser(), $config->getPassword());
				$this->PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
			catch(PDOException $Exception) {
				$this->lastErrorNo = mysql_errno($this->PDO);
				$this->lastErrorMessage = mysql_error($this->PDO);
				echo "<span>$this->lastErrorNo : $this->lastErrorMessage</span>";
			}
		}
		else {
				echo "<span>Aucun type de connection définie dans config.php</span>";
		}
	}

	public function destruct() {
		if($config->getDriver() == 'PDO') {
			$this->_disconnect('pdo');
		}
		else {
			echo "<span>Aucun type de connection définie dans config.php</span>";
		}
	}

	private function _disconnect($type) {
		$PDO = NULL;
		$PDOstatement = NULL;
	}

	public function getPDO() {
		return $PDO;
	}

	public function execute($query) {
		$this->PDOstatement = $this->PDO->query($query);
		return $this->PDOstatement;
	}

	public function select_simple($column, $table) {
		$query = "SELECT " . $column . " FROM " . $table;
		$this->PDOStatement = $this->execute($query);
		return $this->PDOStatement;
	}

	 /**
	 * Fonction de selection de ligne dans la table
 	 * @param String $columns les colonnes séparer par des virgules
 	 * @param String $table nom de la table
 	 * @param String $condition condition
 	 * @param String $orderBy ordre d'affichage
 	 * @param String $limit limite de résultat
 	 * @return Object PDOStatement
 	 */
	public function select($columns, $table, $condition, $orderBy, $limit) {
		$this->execute("SET CHARACTER SET utf8");
		$query = "SELECT $columns FROM $table WHERE $condition ORDER BY $orderBy LIMIT $limit";
		// echo '#'.$query;
		$this->PDOStatement = $this->execute($query);

		return $this->PDOStatement;
	}

	/*
	 * @title :		Fonction de selection de ligne dans la table avec un inner join
	 * @param :		$columns -> 	les colonnes séparer par des virgules,
	 * 				$table -> 		nom de la table,
	 * 				$inner -> 		nom de la table à joindre,
	 * 				$condition -> 	condition,
	 * 				$orderBy -> 	ordre d'affichage,
	 * 				$limit -> 		limite de résultat
	 * @author	: Kévin B.
	 * @return	: Retourne un résultat PDOStatement
	 */
	public function select1inner($columns, $table, $inner, $condition, $orderBy, $limit) {
		$this->execute("SET CHARACTER SET utf8");
		$query = "SELECT $columns FROM $table INNER JOIN $inner WHERE $condition ORDER BY $orderBy LIMIT $limit";
		$this->PDOStatement = $this->execute($query);

		return $this->PDOStatement;
	}

	/*
	 * @title :		Fonction pour entrer une ligne dans la table
	 * @param :		$columns -> l'ordre des colonnes, string séparer par des virgules,
	 * 				$table -> nom de la table,
	 * 				$values -> les valeurs sous forme de tableau,
	 * @author	: Kévin B.
	 * @return	: Retourne true si la ligne est inseré.
	 */
	public function insertInto($columns, $table, $values) {
		$query = "INSERT INTO $table ($columns) VALUES (";
		$i = 1;
		foreach($values as $value) {
			if($i==1) {
				$query = $query . "'" . $value . "'";
				$i=0;
			}
			else {
				$query = $query . ", '" . $value . "'";
			}
		}
		$query = $query . ");";
		echo $query;
		$this->PDOStatement = $this->execute($query);
		return $this->PDOStatement;
	}

	// Page : l'url ou est la page
	public function paginatationSQL($options=null) {
		global $config;

		// Je cherche les options de pagination possibles.
		// Si un slash est trouver dans le reste de la chaine, on cherche
		$test = strpos($options, 'page');

		// test si l'utilisateur demande une page précise
		if($test === 0) {
			// une pagination est trouver
			$controller = new Controller();
			$tableauRecherchePage = $controller->slashitation($options, '/');
			$page = $tableauRecherchePage[1];
		}
		else {
			$page = "1";
		}

		// Si la page 2 ou plus est demander, ajoute l'offest à la requête.
		if($page > 1) {
			// integerisation
			$page = intval($page);
			$firstBorne = $page * $config->getPagination();
			$lastBorne = $firstBorne + $config->getPagination();

			$limit = $lastBorne . " OFFSET " . $firstBorne;
			echo $limit;
		}
		else {
			$limit = $config->getPagination();
		}
		return $limit;
	}

	public function getListTable() {
		$this->execute("SET CHARACTER SET utf8");
		$query = "SHOW TABLES";
		$this->PDOStatement = $this->execute($query);
		$result = $this->PDOStatement->fetchAll();
		return $result;
	}

	// Ordre : es ce que un bouton d'ordre a été selectioner
	public function ordreSQL($options) {
		global $config;
		// Je cherche les options d'ordre possibles.
		// Si un slash est trouver dans le reste de la chaine, on cherche
		$test = strpos($options, 'Ordre=');

		// test si l'utilisateur demande un ordre précise
		if(!empty($test) || $test != '' || $test != '0') {
			// une pagination est trouver
			$tableauRechercheOrdre = $this->slashitation($options, 'Ordre=');

			$ordre = $tableauRechercheOrdre[1];
		}
		else {
			$ordre = "id";
		}

		return $ordre;
	}

	public function getColumnTable($table) {
		$this->execute("SET CHARACTER SET utf8");
		$query = "SHOW COLUMNS FROM $table";
		$this->PDOStatement = $this->execute($query);

		return $this->PDOStatement;
	}
}
