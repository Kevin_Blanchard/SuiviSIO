<?php
require_once(ENGINE . 'Models/Model.php');

class Droit extends Model {
	private $options;
	
	public function __construct($options) {
		parent::__construct();
		$this->options = $options;
	}
	
	public function getLesDroits() {
		
		$query = $this->select(
		'*', // Select
		'rights', // From
		"1", // Where
		$this->ordreSQL($this->options), // Order by
		$this->paginatationSQL($this->options)); // Limit
		
		$result = $query->fetchAll();
		
		return $result;
	}
	
	public function getUnDroit($id) {
		
		// Première requête pour connaitre le nombre de ligne dans la base
		$query = $this->select(
		'count(*)', // Select
		'rights', // From
		"user_id = $id", // Where
		"id", // Order by
		"2"); // Limit
		
		if($query->rowCount() == 1) {
			// Deusième requête pour les résultat
			$query = $this->select(
			'*', // Select
			'rights', // From
			"user_id = $id", // Where
			"id", // Order by
			"1"); // Limit
			
			$result = $query->fetchAll();
		}
		else {
			echo 'Erreur : pas ou trop de ligne(s)';
		}
		return $result;
	}
}

?>
