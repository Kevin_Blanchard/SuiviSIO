<?php

/* Class principale :
 * A appeler sur TOUTES les classs */

abstract class Model extends SQL {
	// Attributs
	protected $primary	= '';
	protected $table	= '';
	protected $display	= '';

	// Methods
	public function __construct() {
		parent::__construct();
		$this->primary	= '';
		$this->table	= '';
		$this->display	= '';
	}

	public function create() {
		// Création de la chaîne pour entrer les infos dans la base, la class fille a créer préalablement la chaîne des colonnes et des datas.
		$sql = "INSERT INTO {$this->table}(({$this->stringCreate}) VALUES ({$this->stringCreateData})";
		$this->execute($sql);
	}

	public function read($id) {

		$sql = "SELECT * FROM {$this->table} WHERE id = {$this->primary}";

		$result = $this->execute($sql);

		$result = $result->fetch_assoc();

		foreach($result as $key => $value) {
			$this->key = $value;
		}
	}

	public function update($data, $column, $table, $condition) {
		if(is_string($data)) {
			$sql = "UPDATE {$table} SET {$column} = '{$data}' WHERE {$condition}";
		}
		else {
			$sql = "UPDATE {$table} SET {$column} = {$data} WHERE {$condition}";
		}

		return $this->execute($sql);
	}

	public function delete() {
		$sql = "DELETE FROM {$this->table} WHERE id = {$this->primary}";
		$this->execute($sql);
	}

	public function find($condition) {
		$sql = "SELECT * FROM {$this->table} WHERE {$condition}";
		$this->execute($sql);
	}

	/**
	 * @param String
	 * @return String
	 */
	public function getNbPage($table) {
		$PDO = new SQL();
		$query = $PDO->select(
		'count(*)', // Select
		$table, // From
		'1', // Where
		'1', // Order by
		'1'); // Limit
		$result = $query->fetchAll();
		return $result;
	}
}

?>
