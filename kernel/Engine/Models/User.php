<?php
require_once(ENGINE . 'Models/Model.php');

class User extends Model {
	private $options;

	public function __construct($options) {
		parent::__construct();
		$this->options = $options;
	}

	public function getLesUser() {

		$query = $this->select(
		'*', // Select
		'users', // From
		"1", // Where
		$this->ordreSQL($this->options), // Order by
		$this->paginatationSQL($this->options)); // Limit

		$result = $query->fetchAll();

		return $result;
	}

	public function getUnUser($id) {

		// Première requête pour connaitre le nombre de ligne dans la base
		$query = $this->select(
		'count(*)', // Select
		'users', 	// From
		"1", 		// Where
		"id", 		// Order by
		"1"); 		// Limit

		if($query->rowCount() == 1) {
			// Deusième requête pour les résultat
			$query = $this->select(
			'*', 			// Select
			'users',		// From
			"id = $id", 	// Where
			"id", 			// Order by
			"1"); 			// Limit

			$result = $query->fetchAll();
		}
		else {
			echo 'Erreur : pas ou trop de ligne(s)';
		}
		return $result;
	}

	public function deleteUser($id) {
		$this->primary = $id;
		$this->delete();
		$_SESSION = NULL;
	}

	public function updateUser($username, $email, $password=NULL, $nom, $prenom) {

		$this->primary = $_SESSION['idUser'];
		$id = $_SESSION['idUser'];
		$upDate = date('Y-m-d h:i:s');

		$test = $this->update($username,'username', 'users', 'id = '.$id);

		// Si le pseudo a changer, le mettre à jour dans la variable de session
		if($test) {
			$_SESSION['nameUser'] = $username;
		}

		$this->update($email, 'email', 'users', 'id = '.$id);

		// Condition pour ne pas écraser le mot de passe si l'utilisateur n'a pas remplie les 2 champs
		if($password != NULL) {
			$this->update(md5($password),'password', 'users', 'id = '.$id);
		}

		$this->update($prenom,  'prenom',   'users', 'id = '.$id);
		$this->update($nom,     'nom',      'users', 'id = '.$id);
		$this->update($upDate,  'modified', 'users', 'id = '.$id);

	}
}

?>
