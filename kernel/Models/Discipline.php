<?php
require_once(ENGINE . 'Models/Model.php');

class Discipline extends Model {
	private $options;

	public function __construct($options) {
		$this->options = $options;
	}

	/**
	 * @param String where
	 * @return String
	 */
	public function getLesDisciplines($where) {
		$PDO = new SQL();
		$query = $PDO->select(
		'*', // Select
		'disciplines', // From
		$where, // Where
		$PDO->ordreSQL($this->options), // Order by
		$PDO->paginatationSQL($this->options)); // Limit
		$result = $query->fetchAll();

		// Ajout du nb de métier par discipline
		$lesLiens = $this->getLesDisciplinesMetiers();

		foreach ($lesLiens as $key) {
			foreach ($result as $key2) {
				if($key['discipline_id'] === $key2['id']) {
					$key2['metiers_liens'] = $key['metiers_liens'];
				}
			}
		}
		
		print_r($result);

		return $result;
	}

	public function getLesDisciplinesMetiers() {
		$PDO = new SQL();
		$query = $PDO->select_simple('discipline_id, COUNT(metier_id) as metiers_liens', 'disciplines_metiers GROUP BY discipline_id');
		$result = $query->fetchAll();
		return $result;
	}

	/**
	 * @param String where
	 * @return String
	 */
	public function getUnDiscipline($condition) {
		$PDO = new SQL();
		// Première requête pour connaitre le nombre de ligne dans la base #Vérification
		$query = $PDO->select(
		'count(*)', // Select
		'disciplines', // From
		$condition, // Where
		"id", // Order by
		"1"); // Limit
		if($query->rowCount() == 1) {
			// Deusième requête pour les résultat
			$query = $PDO->select(
			'*', // Select
			'disciplines', // From
			$condition, // Where
			"id", // Order by
			$PDO->paginatationSQL($this->options)); // Limit
			$result = $query->fetchAll();
		}
		else {
			echo 'Erreur : pas ou trop de ligne(s)';
		}
		return $result;
	}

	public function getNbPageDisciplines() {
		return $this->getNbPage('disciplines');
	}
}

?>
