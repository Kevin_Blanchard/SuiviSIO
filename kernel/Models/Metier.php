<?php
require_once(ENGINE . 'Models/Model.php');

class Metier extends Model {
	private $options;

	public function __construct($options) {
		$this->options = $options;
	}

	/**
	 * @param String where
	 * @return String
	 */
	public function getLesMetiers($where) {
		$PDO = new SQL();
		$query = $PDO->select(
		'*', // Select
		'metiers', // From
		$where, // Where
		$PDO->ordreSQL($this->options), // Order by
		$PDO->paginatationSQL($this->options)); // Limit
		$result = $query->fetchAll();
		return $result;
	}

	/**
	 * @param String where
	 * @return String
	 */
	public function getUnMetier($condition) {
		$PDO = new SQL();
		// Première requête pour connaitre le nombre de ligne dans la base
		$query = $PDO->select(
		'count(*)', // Select
		'metiers', // From
		$condition, // Where
		"id", // Order by
		"1"); // Limit
		if($query->rowCount() == 1) {
			// Deusième requête pour les résultat
			$query = $PDO->select(
			'*', // Select
			'metiers', // From
			$condition, // Where
			"id", // Order by
			$PDO->paginatationSQL($this->options)); // Limit
			$result = $query->fetchAll();
		}
		else {
			echo 'Erreur : pas ou trop de ligne(s)';
		}
		return $result;
	}

	public function getNbPageMetier() {
		return $this->getNbPage('metiers');
	}
}

?>
