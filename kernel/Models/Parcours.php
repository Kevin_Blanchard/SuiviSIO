<?php
require_once(ENGINE . 'Models/Model.php');

class Parcours extends Model {
	private $options;
	
	public function __construct($options) {
		$this->options = $options;
	}
	
	public function getLesParcours() {
		
		$PDO = new SQL();
		
		$query = $PDO->select(
		'*', // Select
		'parcours', // From
		"1", // Where
		$PDO->ordreSQL($this->options), // Order by
		$PDO->paginatationSQL($this->options)); // Limit
		
		$result = $query->fetchAll();
		
		return $result;
	}
	
	public function getUnParcours($id) {
		
		$PDO = new SQL();
		
		// Première requête pour connaitre le nombre de ligne dans la base
		$query = $PDO->select(
		'count(*)', // Select
		'parcours', // From
		"1", // Where
		"id", // Order by
		"1"); // Limit
		
		if($query->rowCount() == 1) {
			// Deusième requête pour les résultat
			$query = $PDO->select(
			'*', // Select
			'parcours', // From
			"1", // Where
			"id", // Order by
			$PDO->paginatationSQL($this->options)); // Limit
			
			$result = $query->fetchAll();
		}
		else {
			echo 'Erreur : pas ou trop de ligne(s)';
		}
		return $result;
	}
}

?>
