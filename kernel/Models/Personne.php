<?php

class Personne extends Model {
	private $id;
	private $nom;
	private $prenom;
	private $mail;
	private $section_id;
	private $statut_id;
	private $universite_id;
	private $created;
	private $modified;
	
	public function __construct() {
		$this->id 				= $id;
		$this->nom 				= $nom;
		$this->prenom 			= $prenom;
		$this->mail 			= $mail;
		$this->section_id 		= $section_id;
		$this->statut_id 		= $statut_id;
		$this->universite_id 	= $universite_id;
		$this->created 			= $created;
		$this->modified 		= $modified;
	}
	
	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getNom(){
		return $this->nom;
	}

	public function setNom($nom){
		$this->nom = $nom;
	}

	public function getPrenom(){
		return $this->prenom;
	}

	public function setPrenom($prenom){
		$this->prenom = $prenom;
	}

	public function getMail(){
		return $this->mail;
	}

	public function setMail($mail){
		$this->mail = $mail;
	}

	public function getSection_id(){
		return $this->section_id;
	}

	public function setSection_id($section_id){
		$this->section_id = $section_id;
	}

	public function getStatut_id(){
		return $this->statut_id;
	}

	public function setStatut_id($statut_id){
		$this->statut_id = $statut_id;
	}

	public function getUniversite_id(){
		return $this->universite_id;
	}

	public function setUniversite_id($universite_id){
		$this->universite_id = $universite_id;
	}

	public function getCreated(){
		return $this->created;
	}

	public function setCreated($created){
		$this->created = $created;
	}

	public function getModified(){
		return $this->modified;
	}

	public function setModified($modified){
		$this->modified = $modified;
	}
}

?>
