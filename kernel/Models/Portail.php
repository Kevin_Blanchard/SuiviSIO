<?php

class Portail extends Model {
	private $id;
	private $nom;
	private $universite_id;
	private $created;
	private $modified;
	
	public function __construct() {
		$this->id 				= $id;
		$this->nom 				= $nom;
		$this->universite_id 	= $universite_id;
		$this->created 			= $created;
		$this->modified 		= $modified;
	}
	
	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getNom(){
		return $this->nom;
	}

	public function setNom($nom){
		$this->nom = $nom;
	}

	public function getUniversite_id(){
		return $this->universite_id;
	}

	public function setUniversite_id($universite_id){
		$this->universite_id = $universite_id;
	}

	public function getCreated(){
		return $this->created;
	}

	public function setCreated($created){
		$this->created = $created;
	}

	public function getModified(){
		return $this->modified;
	}

	public function setModified($modified){
		$this->modified = $modified;
	}
}

?>
