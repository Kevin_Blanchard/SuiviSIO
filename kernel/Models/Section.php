<?php

class Section extends Model {
	private $id;
	private $nom;
	private $numero;
	private $discipline_id;
	private $created;
	private $modified;
	
	public function __construct() {
		$this->id 		= $id;
		$this->nom 		= $nom;
		$this->numero 	= $numero;
		$this->discipline_id 		= $discipline_id;
		$this->created 	= $created;
		$this->modified = $modified;
	}
	
	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getNom(){
		return $this->nom;
	}

	public function setNom($nom){
		$this->nom = $nom;
	}

	public function getNumero(){
		return $this->numero;
	}

	public function setNumero($numero){
		$this->numero = $numero;
	}

	public function getDiscipline_id(){
		return $this->discipline_id;
	}

	public function setDiscipline_id($discipline_id){
		$this->discipline_id = $discipline_id;
	}

	public function getCreated(){
		return $this->created;
	}

	public function setCreated($created){
		$this->created = $created;
	}

	public function getModified(){
		return $this->modified;
	}

	public function setModified($modified){
		$this->modified = $modified;
	}
}

?>
