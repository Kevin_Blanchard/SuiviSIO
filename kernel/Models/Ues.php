<?php

class Ues extends Model {
	private $id;
	private $nom;
	private $codeapogee;
	private $hcours;
	private $htd;
	private $htp;
	private $ects;
	private $contenu;
	private $prerequis;
	private $universite_id;
	private $lieu_id;
	private $personne_id;
	private $objectif;
	private $hetu;
	private $gpcours;
	private $gptd;
	private $gptp;
	private $contents;
	private $aim;
	private $created;
	private $modified;
	
	public function __construct() {
		$this->id 				= $id;
		$this->nom 				= $nom;
		$this->codeapogee 		= $codeapogee;
		$this->hcours 			= $hcours;
		$this->htd 				= $htd;
		$this->htp 				= $htp;
		$this->ects 			= $ects;
		$this->contenu 			= $contenu;
		$this->prerequis 		= $prerequis;
		$this->universite_id 	= $universite_id;
		$this->lieu_id 			= $lieu_id;
		$this->personne_id 		= $objectif;
		$this->hetu 			= $hetu;
		$this->gpcours 			= $gpcours;
		$this->gptd 			= $gptd;
		$this->gptp 			= $gptp;
		$this->contents 		= $contents;
		$this->aim 				= $aim;
		$this->created 			= $created;
		$this->modified 		= $modified;
	}
	
	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getNom(){
		return $this->nom;
	}

	public function setNom($nom){
		$this->nom = $nom;
	}

	public function getCodeapogee(){
		return $this->codeapogee;
	}

	public function setCodeapogee($codeapogee){
		$this->codeapogee = $codeapogee;
	}

	public function getHcours(){
		return $this->hcours;
	}

	public function setHcours($hcours){
		$this->hcours = $hcours;
	}

	public function getHtd(){
		return $this->htd;
	}

	public function setHtd($htd){
		$this->htd = $htd;
	}

	public function getHtp(){
		return $this->htp;
	}

	public function setHtp($htp){
		$this->htp = $htp;
	}

	public function getEcts(){
		return $this->ects;
	}

	public function setEcts($ects){
		$this->ects = $ects;
	}

	public function getContenu(){
		return $this->contenu;
	}

	public function setContenu($contenu){
		$this->contenu = $contenu;
	}

	public function getPrerequis(){
		return $this->prerequis;
	}

	public function setPrerequis($prerequis){
		$this->prerequis = $prerequis;
	}

	public function getUniversite_id(){
		return $this->universite_id;
	}

	public function setUniversite_id($universite_id){
		$this->universite_id = $universite_id;
	}

	public function getLieu_id(){
		return $this->lieu_id;
	}

	public function setLieu_id($lieu_id){
		$this->lieu_id = $lieu_id;
	}

	public function getPersonne_id(){
		return $this->personne_id;
	}

	public function setPersonne_id($personne_id){
		$this->personne_id = $personne_id;
	}

	public function getObjectif(){
		return $this->objectif;
	}

	public function setObjectif($objectif){
		$this->objectif = $objectif;
	}

	public function getHetu(){
		return $this->hetu;
	}

	public function setHetu($hetu){
		$this->hetu = $hetu;
	}

	public function getGpcours(){
		return $this->gpcours;
	}

	public function setGpcours($gpcours){
		$this->gpcours = $gpcours;
	}

	public function getGptd(){
		return $this->gptd;
	}

	public function setGptd($gptd){
		$this->gptd = $gptd;
	}

	public function getGptp(){
		return $this->gptp;
	}

	public function setGptp($gptp){
		$this->gptp = $gptp;
	}

	public function getContents(){
		return $this->contents;
	}

	public function setContents($contents){
		$this->contents = $contents;
	}

	public function getAim(){
		return $this->aim;
	}

	public function setAim($aim){
		$this->aim = $aim;
	}

	public function getCreated(){
		return $this->created;
	}

	public function setCreated($created){
		$this->created = $created;
	}

	public function getModified(){
		return $this->modified;
	}

	public function setModified($modified){
		$this->modified = $modified;
	}
}

?>
