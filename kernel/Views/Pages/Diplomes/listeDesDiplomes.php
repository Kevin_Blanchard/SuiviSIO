<div  class="container">
	<div class="page-header">
		<div class="row">
			<h1>Liste des diplomes</h1>
			<a href="<?php echo WEBROOT; ?>" class="btn btn-default">Retour à l'Accueil</a>
		</div>
	</div>

	<div class="col-lg-12 col-md-7 col-sm-6">
		<div class="row">
			<table class="table table-striped table-hover">
			<thead>
				<tr>
					<td>id</td>
					<td>nom</td>
					<td>Nombres de semestre minimum</td>
					<td>Nombres de semestre maximum</td>
				</tr>
			</thead>
			<?php /* Affiche les métiers et leur description */ ?>
			<?php foreach($lesDiplomes as $unDiplome): ?>
			<tbody>
				<tr>
					<td><?php echo $unDiplome['id']; ?></td>
					<td><a href="<?php echo WEBROOT; ?>Action/Diplome/id/<?php echo $unDiplome['id']; ?>">
						<?php echo $unDiplome['nom']; ?>
					</a></td>
					<td><?php echo $unDiplome['semestre_min']; ?></td>
					<td><?php echo $unDiplome['semestre_max']; ?></td>
				</tr>
			</tbody>
			<?php endforeach; ?>
			</table>
			<?php include(TEMPLATE . 'pagination.php') ?>
		</div>
	</div>
</div>
