<div  class="container">
	<div class="page-header">
		<div class="row">
			<h1>Liste des Disciplines</h1>
			<a href="<?php echo WEBROOT; ?>" class="btn btn-default">Retour à l'Accueil</a>
		</div>
	</div>

	<div class="col-lg-12 col-md-7 col-sm-6">
		<div class="row">
			<table class="table table-striped table-hover">
			<thead>
				<tr>
					<td>id</td>
					<td>nom</td>
					<td>Nombre de métiers liés à la discipline</td>
				</tr>
			</thead>
			<?php /* Affiche les métiers et leur description */ ?>
			<?php foreach($lesDisciplines as $unDiscipline): ?>
			<tbody>
				<tr>
					<td><?php echo $unDiscipline['id']; ?></td>
					<td><a href="<?php echo WEBROOT; ?>Action/Discipline/id/<?php echo $unDiscipline['id']; ?>">
						<?php echo $unDiscipline['nom']; ?>
					</a></td>
					<td><?php echo $unDiscipline['metier_liers']; ?></td>
				</tr>
			</tbody>
			<?php endforeach; ?>
			</table>
		</div>
	</div>
</div>
