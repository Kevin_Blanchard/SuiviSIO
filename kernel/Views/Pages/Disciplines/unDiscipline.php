<div  class="container">
	<div class="page-header">
		<div class="row">
			<h1>Liste des métiers</h1>
		</div>
	</div>
	<div class="col-lg-12 col-md-7 col-sm-6">
		<div class="row">

					<a href="<?php echo WEBROOT; ?>Action/Metier" class="btn btn-default">Retour à la liste des disciplines</a>
					<h2><?php echo $leMetier[0]['nom']; ?></h2>

					<div class="col-lg-6">
						<h3>Introduction</h3>
						<p><?php echo $leMetier[0]['accroche']; ?></p>
					</div>

					<div class="col-lg-6">
						<h3>Liste des compétences nécaissaires</h3>
						<p><?php echo $leMetier[0]['compliste']; ?></p>
					</div>
		</div>
	</div>
</div>
