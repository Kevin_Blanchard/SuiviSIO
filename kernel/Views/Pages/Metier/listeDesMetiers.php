<div  class="container">
	<div class="page-header">
		<div class="row">
			<h1>Liste des métiers</h1>
			<a href="<?php echo WEBROOT; ?>" class="btn btn-default">Retour à l'Accueil</a>
		</div>
	</div>

	<div class="col-lg-12 col-md-7 col-sm-6">
		<div class="row">
			<table class="table table-striped table-hover">
			<thead>
				<tr>
					<td>id</td>
					<td>nom</td>
					<td>accroche</td>
				</tr>
			</thead>
			<?php /* Affiche les métiers et leur description */ ?>
			<?php foreach($lesMetiers as $unMetier): ?>
			<tbody>
				<tr>
					<td><?php echo $unMetier['id']; ?></td>
					<td><a href="<?php echo WEBROOT; ?>Action/Metier/id/<?php echo $unMetier['id']; ?>">
					<?php echo $unMetier['nom']; ?>
					</a></td>
					<td><?php echo $unMetier['accroche']; ?></td>
				</tr>
			</tbody>
			<?php endforeach; ?>
			</table>
			<?php include(TEMPLATE . 'pagination.php') ?>
		</div>
	</div>
</div>
