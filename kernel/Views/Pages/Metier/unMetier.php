<div  class="container">
	<div class="page-header">
		<div class="row">
			<h1>Liste des métiers</h1>
		</div>
	</div>
	<div class="col-lg-12 col-md-7 col-sm-6">
		<div class="row">
			
					<a href="<?php echo WEBROOT; ?>Action/Metier" class="btn btn-default">Retour à la liste des métiers</a>
					<h2><?php echo $leMetier[0]['nom']; ?></h2>
					
					<div class="col-lg-6">
						<h3>Introduction</h3>
						<p><?php echo $leMetier[0]['accroche']; ?></p>
						
						<h3>Formation</h3>
						<p><?php echo $leMetier[0]['formation']; ?></p>
						
						<h3>Format du Diplome</h3>
						<p><?php echo $leMetier[0]['formatdiplome']; ?></p>
						
						<h3>formathorsregion (?)</h3>
						<p><?php echo $leMetier[0]['formathorsregion']; ?></p>
						
						<h3>Compétences</h3>
						<p><?php echo $leMetier[0]['competences']; ?></p>
					</div>
					
					<div class="col-lg-6">
						<h3>Liste des compétences nécaissaires</h3>
						<p><?php echo $leMetier[0]['compliste']; ?></p>
						
						<h3>Compétences pour l'emploie</h3>
						<p><?php echo $leMetier[0]['compemploi']; ?></p>
						
						<h3>Les Emplois de Demain</h3>
						<p><?php echo $leMetier[0]['emploidemain']; ?></p>
						
						<h3>Les Emplois en Chiffre</h3>
						<p><?php echo $leMetier[0]['emploichiffre']; ?></p>
						
						<h3>Le savez-vous ?</h3>
						<p><?php echo $leMetier[0]['savezvous']; ?></p>
					</div>
		</div>
	</div>
</div>
