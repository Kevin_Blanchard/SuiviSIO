<div  class="container">
	<div class="page-header">
		<div class="row">
			<h1>Liste des secteurs</h1>
		</div>
	</div>
	
	<div class="col-lg-6 col-md-7 col-sm-6">
		<div class="row">
			<table class="table table-striped table-hover">
			<thead>
				<tr>
					<td>id</td>
					<td>nom</td>
				</tr>
			</thead>
			<?php foreach($lesSecteurs as $unSecteur): ?>
			<tbody>
				<tr>
					<td><?php echo $unSecteur['id']; ?></td>
					<td><a href="Secteur/Id/<?php echo $unSecteur['id']; ?>"><?php echo $unSecteur['nom']; ?></a></td>
				</tr>
			</tbody>
			<?php endforeach; ?>
			</table>
		</div>
	</div>
</div>
