<div  class="container">
	<div class="page-header">
		<div class="row">
			<h1>Secteur : <?php echo $leSecteur[0]['nom'];  ?></h1>
		</div>
	</div>
	
	<div class="col-lg-12 col-md-7 col-sm-6">
		<div class="row">
			<table class="table table-striped table-hover ">
			<thead>
				<tr>
					<td>id</td>
					<td>synonid</td>
					<td>accroche</td>
					<td>formation</td>
					<td>formatdiplome</td>
					<td>formathorsregion</td>
					<td>competences</td>
					<td>compliste</td>
					<td>compemploi</td>
					<td>emploidemain</td>
					<td>emploichiffre</td>
					<td>savezvous</td>
				</tr>
			</thead>
			<?php foreach($lesMetiers as $unMetier): ?>
			<tbody>
				<tr>
					<td><?php echo $unMetier['id']; ?></td>
					<td><?php echo $unMetier['synonid']; ?></td>
					<td><?php echo $unMetier['accroche']; ?></td>
					<td><?php echo $unMetier['formation']; ?></td>
					<td><?php echo $unMetier['formatdiplome']; ?></td>
					<td><?php echo $unMetier['formathorsregion']; ?></td>
					<td><?php echo $unMetier['competences']; ?></td>
					<td><?php echo $unMetier['compliste']; ?></td>
					<td><?php echo $unMetier['compemploi']; ?></td>
					<td><?php echo $unMetier['emploidemain']; ?></td>
					<td><?php echo $unMetier['emploichiffre']; ?></td>
					<td><?php echo $unMetier['savezvous']; ?></td>
				</tr>
			</tbody>
			<?php endforeach; ?>
			</table>
		</div>
	</div>
</div>
