<div  class="container">
	<div class="page-header">
		<div class="row">
			<h1>Liste des universites</h1>
		</div>
	</div>
	
	<div class="col-lg-12 col-md-7 col-sm-6">
		<div class="row">
			<table class="table table-striped table-hover ">
			<thead>
				<tr>
					<td>id</td>
					<td>nom</td>
					<td>description</td>
					<td>insertion</td>
					<td>mention_id</td>
					<td>pro</td>
					<td>created</td>
					<td>modified</td>
				</tr>
			</thead>
			<?php foreach($lesParcours as $unParcours): ?>
			<tbody>
				<tr>
					<td><?php echo $unParcours['id']; ?></td>
					<td><?php echo $unParcours['nom']; ?></td>
					<td><?php echo $unParcours['description']; ?></td>
					<td><?php echo $unParcours['insertion']; ?></td>
					<td><?php echo $unParcours['mention_id']; ?></td>
					<td><?php echo $unParcours['pro']; ?></td>
					<td><?php echo $unParcours['created']; ?></td>
					<td><?php echo $unParcours['modified']; ?></td>
				</tr>
			</tbody>
			<?php endforeach; ?>
			</table>
		</div>
	</div>
</div>
