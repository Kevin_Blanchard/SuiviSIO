<div  class="container">
	<div class="page-header">
		<div class="row">
			<h1>Liste des universites</h1>
		</div>
	</div>
	
	<div class="col-lg-12 col-md-7 col-sm-6">
		<div class="row">
			<table class="table table-striped table-hover ">
			<thead>
				<tr>
					<td>id</td>
					<td>nom</td>
					<td>ufr</td>
					<td>adresse</td>
					<td>master</td>
					<td>licence</td>
					<td>siteweb</td>
					<td>siteinscriptions</td>
					<td>email</td>
					<td>active</td>
					<td>ordre</td>
					<td>created</td>
					<td>modified</td>
				</tr>
			</thead>
			<?php foreach($lesUniv as $uneUniv): ?>
			<tbody>
				<tr>
					<td><?php echo $uneUniv['id']; ?></td>
					<td><?php echo $uneUniv['nom']; ?></td>
					<td><?php echo $uneUniv['ufr']; ?></td>
					<td><?php echo $uneUniv['adresse']; ?></td>
					<td><?php echo $uneUniv['master']; ?></td>
					<td><?php echo $uneUniv['licence']; ?></td>
					<td><a target="_bank" href="<?php echo $uneUniv['siteweb']; ?>"><?php echo $uneUniv['siteweb']; ?></a></td>
					<td><a target="_bank" href="<?php echo $uneUniv['siteinscriptions']; ?>"><?php echo $uneUniv['siteinscriptions']; ?></a></td>
					<td><?php echo $uneUniv['email']; ?></td>
					<td><?php echo $uneUniv['active']; ?></td>
					<td><?php echo $uneUniv['ordre']; ?></td>
					<td><?php echo $uneUniv['created']; ?></td>
					<td><?php echo $uneUniv['modified']; ?></td>
				</tr>
			</tbody>
			<?php endforeach; ?>
			</table>
		</div>
	</div>
</div>
