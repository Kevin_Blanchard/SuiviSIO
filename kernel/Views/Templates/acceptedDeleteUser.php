<div class="container">
	
	<div class="page-header">
		<div class="row">
			<h1>Votre profil</h1>
		</div>
	</div>
	
	<div class="col-lg-7 col-md-7 col-sm-6">
		<div class="alert alert-dismissible alert-danger">
			<strong>Attention !</strong> Vous allez supprimer l'integraliter des données concernant ce compte.
		</div>
		<div class="row">
			<form action="<?php echo WEBROOT; ?>Action/User/Profil/PostDeleteUser" method="post" class="form-horizontal">
				<fieldset>
					<input type="hidden" name="idDeSuppression" value="<?php echo $_SESSION['idDeSuppression'] ?>">
					<div class="form-group">
					  <div class="col-lg-8 col-lg-offset-2">
						<a href="<?php echo WEBROOT . 'Action/User/Profil'; ?>" class="btn btn-default">Annuler</a>
						<button type="submit" class="btn btn-danger">Supprimer</button>
					  </div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
