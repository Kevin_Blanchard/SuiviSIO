<div  class="container">
	
	<div class="page-header">
		<div class="row">
			<h1>Ajouter un élément "<?php echo $table; ?>"</h1>
		</div>
	</div>
	
	<div class="col-lg-6">
		<div class="row">
			<form action="<?php echo WEBROOT; ?>Action/Administration/PostAdd" method="POST" class="well form-horizontal">
				<fieldset>
					<legend></legend>
					<?php
					/* les inputs */
					// switch pour sauter l'id
					array_shift($columns);
					
					//Pour chaque champ
					foreach($columns as $ligne) {
						echo '<div class="form-group">';
							echo '<label for="title" class="col-lg-2 control-label">'. $ligne['Field'] .'</label>';
							echo '<div class="col-lg-10">';
							
							/* Type INTEGER */
							if(!empty(preg_grep("/^int/", explode("\n", $ligne['Type'])))) {
								echo '<input class="form-control" id="'. $ligne['Field'] .'" placeholder="Nombre" type="number" size="'. 10 .'"></input>';
								if(($ligne['Default'] == -1) && ($ligne['Null'] == 'NO')) {
									echo '<span class="text-danger help-block">Obligatoire</span>';
								}
							}
							
							/* Type VARCHAR */
							if(!empty(preg_grep("/^varchar/", explode("\n", $ligne['Type'])))) {
								echo '<input class="form-control" id="'. $ligne['Field'] .'" placeholder="Text" type="text" size="'. 255 .'"></input>';
								if($ligne['Default'] == -1 && $ligne['Null'] == 'NO') {
									echo '<span class="text-danger help-block">Obligatoire</span>';
								}
							}
							
							/* Type DATE */
							if(!empty(preg_grep("/^datetime/", explode("\n", $ligne['Type']))) || $ligne['Type'] == "smallint(6)") {
								echo '<input class="form-control" id="'. $ligne['Field'] .'" placeholder="Date" type="datetime"></input>';
								if($ligne['Default'] == -1 && $ligne['Null'] == 'NO') {
									echo '<span class="text-danger help-block">Obligatoire</span>';
								}
							}
							
							/* Type REEL */
							if(!empty(preg_grep("/^foat/", explode("\n", $ligne['Type'])))) {
								echo '<input class="form-control" id="'. $ligne['Field'] .'" placeholder="Nombre a virgulle" type="foat"></input>';
								if($ligne['Default'] == -1 && $ligne['Null'] == 'NO') {
									echo '<span class="text-danger help-block">Obligatoire</span>';
								}
							}
							
							/* Type BOOLEEN */
							if(!empty(preg_grep("/^foat/", explode("\n", $ligne['Type'])))) {
								echo '<input class="form-control" id="'. $ligne['Field'] .'" type="checkbox"> Oui</input>';
								if($ligne['Default'] == -1 && $ligne['Null'] == 'NO') {
									echo '<span class="text-danger help-block">Obligatoire</span>';
								}
							}
							
							/* Type TEXTE */
							if(!empty(preg_grep("/^foat/", explode("\n", $ligne['Type'])))) {
								echo '<input class="form-control" id="'. $ligne['Field'] .'" type="text"> Oui</input>';
								if($ligne['Default'] == -1 && $ligne['Null'] == 'NO') {
									echo '<span class="text-danger help-block">Obligatoire</span>';
								}
							}
							echo '</div>';
						echo '</div>';
					}
					?>
					<div class="form-group">
					  <div class="col-lg-8 col-lg-offset-2">
						<a href="<?php echo WEBROOT . 'Action/Administration'; ?>" class="btn btn-danger">Annuler</a>
						<button type="submit" class="btn btn-success">Envoyer</button>
					  </div>
					</div>
					
				</fieldset>
			</form>
		</div>
	</div>
	
</div>
