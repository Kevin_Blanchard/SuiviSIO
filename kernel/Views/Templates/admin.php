<nav class="navbar navbar-default navbar-fixed-top" style="top:54px;">
  <div class="container-fluid">

	<div class="collapse navbar-collapse">
		<ul class="nav navbar-nav">
			<form class="navbar-form navbar-left" role="recherche">
				<div class="form-group">
					<input class="form-control" placeholder="Rechercher" type="text">
					<select class="form-control">
						<?php foreach($lesTables as $unTable): ?>
							<option><?php echo $unTable[0] ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<button type="submit" class="btn btn-default">Rechercher</button>
			</form>
			<?php foreach($lesTables as $unTable): ?>
				<li class="active"><a href="#<?php echo $unTable[0] ?>"><?php echo $unTable[0] ?></a></li>
			<?php endforeach; ?>
		</ul>
    </div>
  </div>
</nav>

<div class="container">
	<div class="col-lg-12" style="padding-top:154px;">
		<?php
		$i = 0;
		
		/* Pour chaque ligne du tableau des titre */
		foreach($lesTables as $unTitreTable) {
			
			/* Affectation pour le tableau suivant */
			$leHeader = $lesAutres[$i][0];
			
			/* Un titre par table */
			echo "<h1>" . $unTitreTable[0] . "</h1>";
			echo "<div id=\"" . $unTitreTable[0] . "\" class=\"col-lg-12\">";
				echo "<table class=\"table table-striped table-hover\">";
				echo '<a href="' . WEBROOT . 'Action/Administration/Add/' . $unTitreTable[0] . '" class="btn btn-info">Ajouter une ligne</a>';
					echo "<thead>";
						echo "<tr>";
							$switch = TRUE;
							while ($keyName = current($leHeader)) {
								if($switch === TRUE) {
									echo '<th>' . key($leHeader) . '</th>';
									$switch = FALSE;
								}
								else {
									$switch = TRUE;
								}
								next($leHeader);
							}
							echo "<th></th>";
							echo "<th></th>";
						echo "</tr>";
					echo "</thead>";
					echo "<tbody>";
						/* Pour chaque tableau, charge ligne par ligne */
						$tempo = $lesAutres[$i];
						foreach($tempo as $uneLigne) {
							$bouton = TRUE;
							echo '<tr>';
							foreach($uneLigne as $uneCase) {
								if($bouton === TRUE) {
									echo '<td>' . $uneCase . '</td>';
									$bouton = FALSE;
								}
								else {
									$bouton = TRUE;
								}
							}
							echo '<td><a href="' . WEBROOT . 'Action/Administration/Remove/' . $unTitreTable[0] . '/' . $uneLigne[0] . '">Supprimer</a></td>';
							echo '<td><a href="' . WEBROOT . 'Action/Administration/Updated/' . $unTitreTable[0] . '/' . $uneLigne[0] . '">Modifier</a></td>';
							echo '</tr>';
						}
					echo "</tbody>";
				echo "</table>";
				include(TEMPLATE . 'pagination.php');
			echo "</div>";
			/* Incrémentation pour le tableau suivant */
			$i++;
		}
		?>
	</div>
</div>
