﻿<!DOCTYPE html>
<html lang="<?php echo $config->getLang(); ?>">
<head>
	<title><?php echo $config->getTitle(); ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link id="css" href="<?php echo CSS . $config->getTheme(); ?>.bootstrap.css" rel="stylesheet" media="all" type="text/css"/> 
	<style id="css2" type="text/css" media="all">@import "<?php echo CSS . $config->getTheme(); ?>.bootstrap.css";</style>

	<!-- JS -->
	<script src="<?php echo JS; ?>jquery-1.10.2.min.js"></script>
	<script src="<?php echo JS; ?>bootstrap.min.js"></script>
</head>
