<div  class="container">

	<div class="page-header">
		<div class="row">
			<h1>Connexion</h1>
		</div>
	</div>

	<div class="col-lg-2 col-md-7 col-sm-6">
	</div>

	<div class="col-lg-6 col-md-7 col-sm-6">
		<div class="row">
			<div class="well bs-component">
				<form class="form-horizontal" action="<?php echo WEBROOT; ?>Action/Login/PostConnexion" method="post" id="login">
					<fieldset>
						<div class="form-group">

							<div class="form-group">
								<label class="col-lg-4 control-label" id="pseudo_label">Pseudo/Email</label>
								<div class="col-lg-8">
									<input id="pseudo" name="pseudo" type="text" class="form-control" placeholder="Mr. Patate" aria-describedby="username">
								</div>
							</div>

							<div class="form-group">
								<label class="col-lg-4 control-label" id="pass_label">Mot de passe</label>
								<div class="col-lg-8">
									<input id="mdp" name="mdp" type="password" class="form-control" placeholder="Les petits four sont cuits" aria-describedby="password">
								</div>
							</div>

						</div>

						<div class="form-group">
							<div class="col-lg-6">
								<div class="btn-group">
									<input class="btn btn-success dropdown-toggle" type="submit" value="Connexion">
								</div>
							</div>

							<div class="col-lg-6">
								<div class="btn-group">
									<input class="btn btn-default dropdown-toggle" type="cancel" value="Annuler">
								</div>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
