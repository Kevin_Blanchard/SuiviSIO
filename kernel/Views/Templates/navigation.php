<body>
	<header>
		<div class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<a href="" class="navbar-brand"><?php echo $config->getTitle(); ?></a>
				</div>
				<button aria-expanded="false" class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#navbar-main">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
