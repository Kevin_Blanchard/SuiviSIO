				<ul class="nav navbar-nav">
					<li><a href="<?php echo WEBROOT; ?>">Accueil</a></li>
					<li>
						<a href="<?php echo WEBROOT; ?>Action/User/Profil" >
							<?php echo 'Bonjour ' . $_SESSION['nameUser']; ?>
						</a>
					</li>
					<?php
						// Si l'utilisateur est administrateur
						if(isset($_SESSION['right'])){
							if($_SESSION['right'][0]['admin'] == 1) {
								echo '<li>';
								echo '	<a href="' . WEBROOT . 'Action/Administration">';
								echo 'Administration';
								echo '	</a>';
								echo '</li>';
							}
						}
					?>
					<li><a class="" href="<?php echo WEBROOT; ?>Action/Logout">Déconnexion</a></li>
				</ul>
			</div>
		</div>
	</header>
