<?php
/*
 * Parametre : nombre d'élements, éléments par page (dans config.php)
 * */
	$config = new conf();
	$leNombreDePages = $leNombreDePages[0]['count(*)'] / $config->getPagination();
?>

	<div>
		<ul class="pagination pagination-sm">
			<?php if($leNombreDePages == 0): ?>
			<li class=""><a href="<?php echo WEBROOT; ?>#">&laquo;</a></li>
			<?php else: ?>
			<li class="disabled"><a href="<?php echo WEBROOT; ?>#">&laquo;</a></li>
			<?php endif; ?>

			<?php
			for($incre=1; $incre <= $leNombreDePages; $incre++) {
				echo '<li class=""><a href="'. 
				WEBROOT . 'Action/'. $controller .'/page/' . $incre .'">'. $incre .'</a></li>';
			}
			?>

			<?php if($leNombreDePages > 2): ?>
			<li class="disabled"><a href="<?php echo WEBROOT; ?>#">&raquo;</a></li>
			<?php else: ?>
			<li><a href="<?php echo WEBROOT . 'Page=' . $leNombreDePages ?>">&raquo;</a></li>
			<?php endif; ?>

		</ul>
	</div>
