<div class="container">
	
	<div class="page-header">
		<div class="row">
			<h1>Votre profil</h1>
		</div>
	</div>
	<div class="col-lg-6 col-md-7 col-sm-6">
	</div>
	
	<div class="col-lg-5 col-md-7 col-sm-6">
		<div class="row">
			<form action="<?php echo WEBROOT; ?>Action/User/Profil/PostProfil" method="POST" class="form-horizontal">
				<fieldset>
					<!-- Pseudo -->
					<div class="form-group">
					  <label for="pseudo" class="col-lg-4 control-label">Pseudo</label>
					  <div class="col-lg-8">
						<input class="form-control" id="pseudo" placeholder="Pseudo" name="username" value="<?php echo $info[0]['username']; ?>" type="text">
					  </div>
					</div>
					<!-- Email -->
					<div class="form-group">
					  <label for="email" class="col-lg-4 control-label">Email</label>
					  <div class="col-lg-8">
						<input class="form-control" id="email" placeholder="Email" name="email" value="<?php echo $info[0]['email']; ?>" type="text">
						<span class="help-block">Cet email est votre identifiant</span>
					  </div>
					</div>
					<!-- Password -->
					<div class="form-group">
					  <label for="password" class="col-lg-4 control-label">Mot de passe</label>
					  <div class="col-lg-8">
						<input class="form-control" id="password" placeholder="azerty1234" type="password" name="password">
					  </div>
					</div>
					<!-- Password confirmation -->
					<div class="form-group">
					  <label for="confirmePassword" class="col-lg-4 control-label">Confirmer le mot de passe</label>
					  <div class="col-lg-8">
						<input class="form-control" id="confirmePassword" placeholder="azerty1234" type="password" name="comfirmepass">
					  </div>
					</div>
					<!-- Prénom -->
					<div class="form-group">
					  <label for="prenom" class="col-lg-4 control-label">Prénom</label>
					  <div class="col-lg-8">
						<input class="form-control" id="prenom" name ="prenom" value="<?php echo $info[0]['prenom']; ?>" type="text">
					  </div>
					</div>
					<!-- Nom -->
					<div class="form-group">
					  <label for="nom" class="col-lg-4 control-label">Nom</label>
					  <div class="col-lg-8">
						<input class="form-control" id="nom" name="nom" value="<?php echo $info[0]['nom']; ?>" type="text">
					  </div>
					</div>
					<!-- Statistiques -->
					<div class="form-group">
						<label class="col-lg-4 control-label">Statistiques</label>
						<div class="col-lg-8">
							<span class="help-block">Dernière modification <?php echo $info[0]['modified']; ?></span>
							<span class="help-block">Date de création du compte <?php echo $info[0]['created']; ?></span>
						</div>
					</div>
					<!-- Suppression compte -->
					<div class="form-group">
						<label class="col-lg-4 control-label"></label>
						<div class="col-lg-8">
							<a href="<?php echo WEBROOT; ?>Action/User/Profil/RemoveUser" class="btn btn-danger">Supprimer votre compte</a>
						</div>
					</div>
					<div class="form-group">
					  <div class="col-lg-8 col-lg-offset-2">
						<button type="reset" class="btn btn-default">Annuler</button>
						<button type="submit" class="btn btn-primary">Envoyer</button>
					  </div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
