<div  class="container">

	<div class="page-header">
		<div class="row">
			<h1>Inscription</h1>
		</div>
	</div>

	<div class="col-lg-2 col-md-7 col-sm-6">
	</div>

	<div class="col-lg-6 col-md-7 col-sm-6">
		<div class="row">
			<div class="well bs-component">
				<form class="form-horizontal" action="<?php echo WEBROOT; ?>Register/PostRegister" method="post" id="register">
					<fieldset>
						<div class="form-group">

							<div class="form-group">
								<label class="col-lg-4 control-label" id="email">Email</label>
								<div class="col-lg-8">
									<input class="form-control" id="email" name="email" type="text" placeholder="Mr. Patate" aria-describedby="email">
								</div>
							</div>

							<div class="form-group">
								<label class="col-lg-4 control-label" id="basic-addon1">Mot de passe</label>
								<div class="col-lg-8">
									<input id="password" name="password" type="password" class="form-control" placeholder="Les petits four sont cuits" aria-describedby="password">
								</div>
							</div>

							<div class="form-group">
								<label class="col-lg-4 control-label" id="basic-addon1">Comfirmer le mot de passe</label>
								<div class="col-lg-8">
									<input id="confirmed" name="confirmed" type="password" class="form-control" placeholder="Les petits four sont cuits et la poel est chaude" aria-describedby="confirmed">
								</div>
							</div>

						</div>
						<div class="form-group">

							<div class="col-lg-6 btn-group">
								<input class="btn btn-success" type="submit" value="Inscription">
							</div>

							<div class="col-lg-6 btn-group">
								<input class="btn btn-default" type="cancel" value="Annuler">
							</div>

						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
